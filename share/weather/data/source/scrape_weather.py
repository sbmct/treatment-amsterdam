import globalmaptiles as tile_bounds
import os, os.path
import requests


TILE_SERVER = 'http://{s}.tile.openweathermap.org/map/precipitation_cls/{z}/{x}/{y}.png&APPID=2b2ea6ea4c0d0ba35e77f14127892db5'
DEST_DIR = os.path.join(os.path.dirname(__file__), 'tiles')
#what zooms?
min_zoom = 4
max_zoom = 6
MIN_LAT = 21
MAX_LAT = 75
MIN_LON = -30
MAX_LON = 60


def deg_bounds_to_tile_bounds(min_lat, max_lat, min_lon, max_lon, zoom):
    tb = tile_bounds.GlobalMercator()

    us_mbl = tb.LatLonToMeters(min_lat, min_lon)
    us_mtr = tb.LatLonToMeters(max_lat, max_lon)

    bl_t = tb.MetersToTile(us_mbl[0], us_mbl[1], zoom)
    tr_t = tb.MetersToTile(us_mtr[0], us_mtr[1], zoom)
    bl_t = tb.GoogleTile(bl_t[0], bl_t[1], zoom)
    tr_t = tb.GoogleTile(tr_t[0], tr_t[1], zoom)
    #the min_y is the top y (y is flipped)
    return bl_t[0], tr_t[0], tr_t[1], bl_t[1]


#subdomains can be a-c for parallel requests
sub_domain = 'a'
for zoom in xrange(min_zoom, max_zoom+1):
    z_dir = os.path.join(DEST_DIR, str(zoom))
    print z_dir
    if not os.path.exists(z_dir):
        os.makedirs(z_dir)
    #convert ll bounds to tile bounds
    #print deg_bounds_to_tile_bounds(MIN_LAT, MAX_LAT, MIN_LON, MAX_LON, zoom)
    min_x, max_x, min_y, max_y = deg_bounds_to_tile_bounds(MIN_LAT, MAX_LAT, MIN_LON, MAX_LON, zoom)
    #scrape tiles
    for x in xrange(min_x, max_x+1):
        for y in xrange(min_y, max_y+1):
            fname = 'x=%s&y=%s.png' % (x, y)
            fpath = os.path.join(z_dir, fname)
            if not os.path.exists(fpath):
                url = TILE_SERVER.format(s=sub_domain, x=x, y=y, z=zoom)
                print url
                while True:
                    r = requests.get(url)
                    if r.status_code == 200:
                        f = open(fpath, 'wb')
                        f.write(r.content)
                        f.close()
                        break
                    else:
                        print r.status_code, r.text

