import math
import os.path
from PIL import Image

DATA_DIR = os.path.join(os.path.dirname(__file__), '..', 'data', 'source')
TILE_DIR = os.path.join(DATA_DIR, 'tiles')

import sys
sys.path.insert(0, DATA_DIR)
import globalmaptiles as tile_bounds

def ll_to_raw_tile(ll_vals, zoom):
    tb = tile_bounds.GlobalMercator()
    mx, my = tb.LatLonToMeters(ll_vals[0], ll_vals[1])
    px, py = tb.MetersToPixels( mx, my, zoom)
    tileSize = 256
    tx = px / float(tileSize) - 1
    ty = py / float(tileSize) - 1
    return tb.GoogleTile(tx, ty, zoom)

def bl_tr_to_tile(bl, tr, zoom):
    left, bottom = ll_to_raw_tile(bl, zoom)
    right, top = ll_to_raw_tile(tr, zoom)
    return bottom, left, top, right

def _intup(num):
    return int(math.ceil(num))

def _intdn(num):
    return int(math.floor(num))

def get_zoom(bl, tr, width):
    #TODO: I consider this a hack
    zoom = 6 #max zoom level - 1
    num_tiles_ref = width / 256.0
    #we will allow the tiles to be resized up to 1.5x
    while zoom >= 6:
        bottom, left, top, right = bl_tr_to_tile(bl, tr, zoom)
        num_tiles_for_zoom = right - left
        if num_tiles_for_zoom < num_tiles_ref:
            #went 1 too far
            zoom += 1
            break
        zoom -= 1
    return zoom


def draw_tiles(bl, tr, width=None, height=None):
    #bl & tr to tile coordinates
    if width is None:
        zwidth = 1000
    else:
        zwidth = width
    zoom = get_zoom(bl, tr, zwidth)
    bottom, left, top, right = bl_tr_to_tile(bl, tr, zoom)

    #generate image from tiles, fully include any tiles the region touches
    dbottom = _intup(bottom)
    dtop = _intdn(top)
    dleft = _intdn(left)
    dright = _intup(right)
    dim_x = dright - dleft
    dim_y = dbottom - dtop

    tile_size = 256
    px_x = dim_x*tile_size
    px_y = dim_y*tile_size
    img = Image.new('RGBA', (px_x, px_y), (0, 0, 0, 0))

    #paste the tiles into the daemon
    base_x = 0
    base_x_px = _intdn(left)
    base_y = 0
    base_y_px = _intdn(top)
    for pos_x in xrange(dleft, dright):
        for pos_y in xrange(dtop, dbottom):
            try:
                rel_path = '%s/x=%s&y=%s.png' % (zoom, pos_x, pos_y)
                p_img = Image.open(os.path.join(TILE_DIR, rel_path))
                draw_base_x = (pos_x - base_x_px-1)*256
                draw_base_y = (pos_y - base_y_px)*256
                img.paste(p_img, (draw_base_x, draw_base_y))
            except IOError:
                pass

    #crop the image so it matches the actual coords
    min_px_x = int((left - dleft) * 256)
    min_px_y = int((top - dtop) * 256)
    max_px_x = int(((dright - right) * 256))
    max_px_y = int(((dbottom - bottom) * 256))
    img = img.crop((min_px_x, min_px_y, px_x-max_px_x, px_y-max_px_y))

    if height is not None and width is not None:
        img = img.resize((width, height), Image.BILINEAR)

    return img



from sluice.texture import Texture, TextureFluoro

class WeatherTexture(Texture):
    def generate(self, p):
        ing = p.ingests()
        width, height = ing['px']
        self.img = draw_tiles(ing['bl'], ing['tr'], width, height)


class WeatherFluoro(TextureFluoro):
    name = 'EuroWeather'
    texture_class = WeatherTexture

def main():
    s = WeatherFluoro()
    s.startup()
    s.run()
    s.quit()

if '__main__' == __name__:
    main()

