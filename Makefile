#Most demos have use python code in a virtualenv, created by make
VENVDO=$(PWD)/scripts/venvdo
OSTYPE := $(shell uname -s)
TREATMENT_ID = $(shell python -c 'import json ; print json.load(file("config.json")).get("id")')
ICONSET = $(PWD)/share/applet.iconset
ICON_SIZES = 16x16 32x32 128x128 256x256 512x512
ICON_FILES = $(foreach size,$(ICON_SIZES),$(ICONSET)/icon_$(size).png)
RETINA_ICON_FILES = $(foreach size,$(ICON_SIZES),$(ICONSET)/icon_$(size)@2.png)
SLUICE_ROOT = /opt/mct/sluice
SLUICE_ICON = $(SLUICE_ROOT)/share/app-icon.png

DEPS := /opt/mct/sluice/deps
PATH := $(DEPS)/bin:$(PATH)
DYLD_FALLBACK_LIBRARY_PATH := $(DEPS)/lib:$(DYLD_FALLBACK_LIBRARY_PATH)
LD_IBRARY_PATH := $(DEPS)/lib:$(LD_LIBRARY_PATH)
LDFLAGS := -L$(DEPS)/lib $(LDFLAGS)
CFLAGS := -I$(DEPS)/include $(CFLAGS)
CPPFLAGS := -I$(DEPS)/include $(CPPFLAGS)
export PATH DYLD_FALLBACK_LIBRARY_PATH LD_LIBRARY_PATH LDFLAGS CFLAGS CPPFLAGS


all: deps icon

icon: icon-$(OSTYPE)

icon-Darwin: | $(HOME)/Desktop/$(TREATMENT_ID).app

$(ICONSET):
	mkdir -p $@

$(PWD)/share/applet.icns: $(ICON_FILES) $(RETINA_ICON_FILES)
	iconutil -c icns --output $@ $(ICONSET)

$(ICONSET)/icon_%@2.png: | $(ICONSET)
	cat $(SLUICE_ICON) | convert -resize `sh -c 'IFS=x read x y <<< "$*" ; echo $$(($$x * 2))x$$(($$y * 2))'` - - > $@

$(ICONSET)/icon_%.png: | $(ICONSET)
	cat $(SLUICE_ICON) | convert -resize $* - - > $@

$(HOME)/Desktop/$(TREATMENT_ID).app: $(PWD)/share/applet.icns
	osacompile -e "do shell script \"$(SLUICE_ROOT)/bin/sluice-run -f -G -D '$(PWD)'\"" -x -o $@
	cp $< $@/Contents/Resources

$(HOME)/.local/share/icons/hicolor/32x32/apps/treatment-$(TREATMENT_ID).png:
	mkdir -p `dirname $@`
	if which convert >>/dev/null 2>&1 ; then convert -resize 32x32 $(SLUICE_ICON) $@ ; elif [ -e "share/icon32.png" ] ; then cp "share/icon32.png" $@ ; fi

icon-Linux: | $(HOME)/Desktop/$(TREATMENT_ID).desktop

$(HOME)/Desktop/$(TREATMENT_ID).desktop: $(HOME)/.local/share/icons/hicolor/32x32/apps/treatment-$(TREATMENT_ID).png
	echo '[Desktop Entry]' > $@.tmp
	echo 'Version=1.0' >> $@.tmp
	echo 'Name=$(TREATMENT_ID)' >> $@.tmp
	echo 'Comment=Sluice Launcher for $(TREATMENT_ID) treatment' >> $@.tmp
	echo 'Icon=treatment-$(TREATMENT_ID)' >> $@.tmp
	echo 'Exec=$(SLUICE_ROOT)/bin/sluice-run -f -G -D "$(PWD)"' >> $@.tmp
	echo 'Terminal=false' >> $@.tmp
	echo 'Type=Application' >> $@.tmp
	echo 'Categories=Application;' >> $@.tmp
	chmod a+x $@.tmp
	mv $@.tmp $@

$(PWD)/share/applet.png:
	if which convertjunk >>/dev/null 2>&1 ; then convert -resize 32x32 $(SLUICE_ICON) $@ ; else cp $(SLUICE_ICON) $@ ; fi

deps: $(PWD)/venv/requirements.txt

$(PWD)/venv/.pip-patch:
	virtualenv $(PWD)/venv
	echo "LS0tIHZlbnYvbGliL3B5dGhvbjIuNy9zaXRlLXBhY2thZ2VzL3BpcC9fdmVuZG9yL3JlcXVlc3RzL3V0aWxzLnB5CTIwMTUtMDgtMjcgMDg6NDk6NTUuMDAwMDAwMDAwIC0wNzAwCisrKyB2ZW52L2xpYi9weXRob24yLjcvc2l0ZS1wYWNrYWdlcy9waXAvX3ZlbmRvci9yZXF1ZXN0cy91dGlscy5weQkyMDE1LTA4LTI3IDA4OjQ4OjQwLjAwMDAwMDAwMCAtMDcwMApAQCAtNDkzLDcgKzQ5Myw3IEBACiAgICAgIyBGaXJzdCBjaGVjayB3aGV0aGVyIG5vX3Byb3h5IGlzIGRlZmluZWQuIElmIGl0IGlzLCBjaGVjayB0aGF0IHRoZSBVUkwKICAgICAjIHdlJ3JlIGdldHRpbmcgaXNuJ3QgaW4gdGhlIG5vX3Byb3h5IGxpc3QuCiAgICAgbm9fcHJveHkgPSBnZXRfcHJveHkoJ25vX3Byb3h5JykKLSAgICBuZXRsb2MgPSB1cmxwYXJzZSh1cmwpLm5ldGxvYworICAgIG5ldGxvYyA9IHVybHBhcnNlKHVybCkubmV0bG9jLnNwbGl0KCdAJywgMSlbLTFdICMjIFJQQwoKICAgICBpZiBub19wcm94eToKICAgICAgICAgIyBXZSBuZWVkIHRvIGNoZWNrIHdoZXRoZXIgd2UgbWF0Y2ggaGVyZS4gV2UgbmVlZCB0byBzZWUgaWYgd2UgbWF0Y2gK=" | base64 -D | patch -p0
	rm -f $(PWD)/venv/lib/python2.7/site-packages/pip/_vendor/requests/utils.pyc
	touch $@

$(PWD)/venv/requirements.txt: requirements.txt | $(PWD)/venv/.pip-patch
	$(VENVDO) pip -vvv install -r $<
	cp $< $@

#
# Cleanup
#

clean:
	rm -fr $(PWD)/venv

